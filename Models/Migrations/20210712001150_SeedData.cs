﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.Sql("Insert Into Contacts Values ('Richard', 'Laine', null), ('Michael', 'Jordan', null)");
            // migrationBuilder.Sql("Insert Into EmailAddress Values (1, 1, '22 Cherry St')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("Delete From Contacts where Id in (1,2)");
            // migrationBuilder.Sql("Delete From EmailAddress where Id = 1");
        }
    }
}
