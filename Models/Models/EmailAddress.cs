﻿namespace Models
{
    public class EmailAddress
    {
        public int Id { get; set; }
        public EmailType EmailType { get; set; }
        public string Address { get; set; }
    }
}