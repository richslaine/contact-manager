﻿using System.Collections.Generic;

namespace Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IList<EmailAddress> Addresses { get; set; }
    }
}