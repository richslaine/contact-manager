﻿using System.Collections;
using System.Collections.Generic;
using Models;

namespace Repository.Interfaces
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetAll();
        int Create(Contact contact);
        public Contact Get(int id);
        void Update(Contact contact);
        public void Delete(int id);

        /// <summary>
        /// Performs the create or update function depending on the need
        /// </summary>
        /// <param name="model">Contact model</param>
        /// <returns>Unique Id of the model</returns>
        int CreateOrUpdate(Contact model)
        {
            if (model.Id == 0)
                return Create(model);
            
            Update(model);
            return model.Id;
        }
    }
}