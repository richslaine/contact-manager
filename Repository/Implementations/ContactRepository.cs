﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.DBContext;
using Repository.Interfaces;

namespace Repository.Implementations
{
    public class ContactRepository : IContactRepository
    {
        private readonly ContactManagerDbContext _dbContext;

        public ContactRepository(ContactManagerDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public IEnumerable<Contact> GetAll()
        {
            return _dbContext.Contacts.Include(a => a.Addresses).ToList();
        }

        public int Create(Contact contact)
        {
            _dbContext.Contacts.Add(contact);
            _dbContext.EmailAddresses.AddRange(contact.Addresses);
            _dbContext.SaveChanges();
            return contact.Id;
        }

        public Contact Get(int id)
        {
            return _dbContext.Contacts.Include(a => a.Addresses).FirstOrDefault(a => a.Id == id);
        }

        public void Update(Contact contact)
        {
            _dbContext.Contacts.Update(contact);

            foreach (var item in contact.Addresses)
            {
                if(item.Id == 0)
                    _dbContext.EmailAddresses.Add(item);
                else
                    _dbContext.EmailAddresses.Update(item);
            }

            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = _dbContext.Contacts.Where(a => a.Id == id);
            _dbContext.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}