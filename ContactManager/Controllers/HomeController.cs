﻿using ContactManager.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Models;
using Models.DBContext;
using Repository.Interfaces;

namespace ContactManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContactRepository _contactRepository;
        private readonly IMapper _mapper;

        public HomeController(IContactRepository contactRepository, IMapper mapper)
        {
            _contactRepository = contactRepository;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var contacts = _contactRepository.GetAll();
            var contactViewModels = _mapper.Map<IEnumerable<Contact>, IEnumerable<ContactViewModel>>(contacts);
            return View(contactViewModels);
        }

        public IActionResult PartialTableView()
        {
            var contacts = _contactRepository.GetAll();
            var contactViewModels = _mapper.Map<IEnumerable<Contact>, IEnumerable<ContactViewModel>>(contacts);

            return PartialView("ContactViewTable", contactViewModels);
        }

        [HttpGet]
        public IActionResult AddEditPartial(int id = 0)
        {
            if (id == 0)
                return PartialView(new ContactViewModel{Addresses = new List<EmailAddressViewModel> {new EmailAddressViewModel()}});
            
            var record = _contactRepository.Get(id);
            var vm = _mapper.Map<Contact, ContactViewModel>(record);

            vm.Addresses ??= new List<EmailAddressViewModel> {new EmailAddressViewModel()};

            return PartialView(vm);
        }
  
        [HttpPost]
        public IActionResult AddEditPartial(ContactViewModel viewModel)
        {
            var model = _mapper.Map<ContactViewModel, Contact>(viewModel);
            _contactRepository.CreateOrUpdate(model);
            return Ok();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
