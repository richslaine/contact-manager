﻿using Microsoft.Extensions.DependencyInjection;
using Repository;
using Repository.Implementations;
using Repository.Interfaces;

namespace ContactManager.StartupHelpers
{
    public static class DependencyInjection
    {
        public static void RegisterDependencies(IServiceCollection services)
        {
            services.AddScoped<IContactRepository, ContactRepository>();
        }
    }
}
