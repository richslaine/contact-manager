﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ContactManager.Models;
using Models;

namespace ContactManager.StartupHelpers
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Contact, ContactViewModel>().ReverseMap();
            CreateMap<EmailAddress, EmailAddressViewModel>().ReverseMap();
        }
	}
}
