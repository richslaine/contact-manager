﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace ContactManager.Models
{
    public class ContactViewModel
    {
        [HiddenInput]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        public IList<EmailAddressViewModel> Addresses { get; set; }
    }
}