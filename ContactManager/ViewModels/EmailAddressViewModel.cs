﻿using Models;
using Repository.Interfaces;

namespace ContactManager.Models
{
    public class EmailAddressViewModel
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public EmailType EmailType { get; set; }
        public string Address { get; set; }
    }
}